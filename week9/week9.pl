use strict;
use warnings;

print "Enter your word:";
my $str =<>;
chomp $str;

if($str =~ /\s+/) {  print "$str contains 0 or more white-spaces\n.";}

if($str =~/^[A-Z]+$/){print "$str it contains upper case \n."; }

if($str =~ /^(\s*|[A-Z]*)(\s|[A-Za-z])*(\s*|[A-Z]*)$/){
print "$str contains combination of capital letters and white-spaces.\n.";}

if($str =~ /[A-Z]\d*/){print "$str contains a capital letter followed by 0 or more digits\n.";}

my $n = <>;
chomp $n;

if($n =~ /\d+\.\d+/) {print "$n contains some digits before and after a decimal point.\n";}

if($n =~ /^\d{1,3}(\.\d{1,3}){3}$/){print "$n might be an ip adress. \n;"}

if($n =~/^[012]/ ){print "$n is an ip adress.\n";}
